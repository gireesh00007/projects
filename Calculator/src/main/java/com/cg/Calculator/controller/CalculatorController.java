package com.cg.Calculator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cg.Calculator.model.Employee;
import com.cg.Calculator.service.CalculatorService;

@RestController
public class CalculatorController {

	@Autowired
	CalculatorService calculatorService;
	
	@RequestMapping(method=RequestMethod.GET, value="/calculator/divisibilityCheck/{no}")
	public String calculateDivisibility(@PathVariable("no") int no) {
		String msg = calculatorService.calculateDivisibility(no);		
		return msg;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/employee", produces= MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Employee getEmployee() {
		Employee emp = new Employee(1,"ABC","IT");
		return emp;	
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/employeeXml", produces= MediaType.APPLICATION_XML_VALUE)
	@ResponseBody
	public Employee getEmployeeXml() {
		Employee emp = new Employee(1,"ABC","IT");
		return emp;	
	}
	
}
