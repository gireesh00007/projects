package com.cg.Calculator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cg.Calculator.dao.DivisibilityCalculatorDao;

@Service
public class CalculatorService {

	@Autowired
	DivisibilityCalculatorDao divisibilityCalculatorDao;
	
	public String calculateDivisibility(int no) {		
		String msg = divisibilityCalculatorDao.calculateDivisibility(no);
		return msg;
	}
	
}
