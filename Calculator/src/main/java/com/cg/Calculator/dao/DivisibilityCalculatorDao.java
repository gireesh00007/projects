package com.cg.Calculator.dao;

import org.springframework.stereotype.Repository;

@Repository
public class DivisibilityCalculatorDao {
	
	public String calculateDivisibility(int no) {
		
	 if(no%2 == 0 && no%4 == 0)	
		 return  no + " is divisible by 2 and 4";
		
	 if(no%2 == 0)	
		 return  no + " is divisible by 2";
	 
	 if(no%4 == 0)	
		 return  no + " is divisible by 4";
	 
	 return "";
		
	}	

}
